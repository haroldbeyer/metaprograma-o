package br.ucsal.pooa.testes;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

import br.ucsal.pooa.modelos.Candidato;
import br.ucsal.pooa.modelos.Validador;

class TesteValidador {

	@Test
	void testValidar() throws IllegalAccessException, Exception {
		Candidato candidato = new Candidato();
		candidato.setCpf("861.420.240-99");
		candidato.setNome("Mario");
		//candidato.setPosicao(30);
		List<String> erros = Validador.validar(candidato);
		assertTrue(erros.contains("Posi��o"));
		assertTrue(erros.contains("Nota"));
		assertFalse(erros.contains("Nome Completo"));
		assertFalse(erros.contains("CPF"));
	}

}
