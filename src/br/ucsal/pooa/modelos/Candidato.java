package br.ucsal.pooa.modelos;

import java.math.BigDecimal;

import br.ucsal.pooa.interfaces.InterfaceValidacao;

public class Candidato {
	@InterfaceValidacao(descricao = "Nome Completo")
	private String nome;
	@InterfaceValidacao(descricao = "CPF")
	private String cpf;
	@InterfaceValidacao(descricao = "Posi��o")
	private Integer posicao;
	//int por padr�o j� se instancia como 0, portanto melhor deixar como Integer
	@InterfaceValidacao(descricao = "Nota")
	private BigDecimal nota;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public int getPosicao() {
		return posicao;
	}
	public void setPosicao(int posicao) {
		this.posicao = posicao;
	}
	public BigDecimal getNota() {
		return nota;
	}
	public void setNota(BigDecimal nota) {
		this.nota = nota;
	}
}
