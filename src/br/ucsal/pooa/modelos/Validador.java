package br.ucsal.pooa.modelos;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.ucsal.pooa.interfaces.InterfaceValidacao;



public class Validador {
	public static List<String> validar(Object object) throws Exception, IllegalAccessException {
		List<String> invalidos = new ArrayList<String>();
		Class<?> classe = object.getClass();
		Field[] atributos = classe.getDeclaredFields();

		for(Field atributo : atributos) {
			atributo.setAccessible(true);
			Object valor = atributo.get(object);
			if(valor == null && atributo.isAnnotationPresent(InterfaceValidacao.class)) {
				InterfaceValidacao validacao = atributo.getAnnotation(InterfaceValidacao.class);
				invalidos.add(validacao.descricao());
			}
			
		}
		
		
		//Exemplo do que n�o fazer abaixo VVV
		/*if(object.getNome() == null) {
			invalidos.add("nome");
//			object.setNome("Mario");
		}
		if(object.getCpf() == null) {
			invalidos.add("cpf");
//			object.setCpf("000.000.000-00");
		}*/
		// Exemplo do que n�o fazer acima ^^^
		return invalidos;
		
	}
	public static void main(String[] args) throws IllegalAccessException, Exception {
		Candidato c = new Candidato();
		c.setNome("Mario");
		c.setPosicao(3);
		c.setCpf("861.492.204-90");
		c.setNota(new BigDecimal(30));
		List<String> lista = Validador.validar(c);
		for(int i=0; i<lista.size(); i++) {
			System.out.println(lista.get(i));
		}
	}
}
